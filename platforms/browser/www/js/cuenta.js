function inasistencia(){
    var desde = $("#desde").val();
    var hasta = $("#hasta").val();
    if(desde != null && hasta != null){
        NativeStorage.getItem(
            "token",
            function(token){ //Success
                $.ajax({
                    url: 'http://localhost/api/token',
                    type: 'post',
                    dataType: 'json',
                    data: {"token": token},
                    success: function(data) {
                        if (data.success) {
                            $.ajax({
                                url: 'http://localhost/api/inasistencia',
                                type: 'post',
                                dataType: 'json',
                                crossDomain: true,
                                data: {"token": token, "desde": desde, "hasta": hasta},
                                success: function(res) {
                                    if(res.success){
                                        $('#inasistencia').modal('close');
                                        cargar('asistencia');
                                        Materialize.toast('Cambios guardados con éxito!', 4000); // 4000 is the duration of the toast
                                    } else {
                                        $('#inasistencia').modal('close');
                                        Materialize.toast(res.message, 4000);
                                    }
                                }
                            });
                        }
                    }
                });
            }, function (error) {
                cerrarsesion();
            }
        );
    }
}

function pagar(){
    var estado = $('#estado option:selected').text().toLowerCase();
    if(estado == 'procesado' || estado == 'pendiente') {
        NativeStorage.getItem(
            "token",
            function(token){ //Success
                $.ajax({
                    url: 'http://localhost/api/token',
                    type: 'post',
                    dataType: 'json',
                    data: {"token": token},
                    success: function(data) {
                        if (data.success) {
                            $.ajax({
                                url: 'http://localhost/api/compra',
                                type: 'post',
                                dataType: 'html',
                                crossDomain: true,
                                data: {"token": token, "estado": estado},
                                success: function(data) {
                                    cargar('comprar');
                                }
                            });
                        } else {
                            cerrarsesion();
                        }
                    }
                });
            }, function (error) {
                cerrarsesion();
            }
        );
    } else {
        $(".input-field label").css("color","red");
    }
}

function cargar(link){
	if (device.platform != 'browser') {
    	$('.button-collapse').sideNav('hide');
    } else if (link != "cuenta") {
    	$('.preloader-background').css("padding-left", "200px");
    }
    $('.preloader-wrapper').delay(300).fadeIn();
    if (link != "cuenta") {
        $('.preloader-background').css("z-index", "900");
    }
	$('.preloader-background').fadeIn('slow',function(){
        NativeStorage.getItem(
                "token",
                function(token){ //Success
                    $.ajax({
                        url: 'http://localhost/api/token',
                        type: 'post',
                        dataType: 'json',
                        data: {"token": token},
                        success: function(data) {
                            if (data.success) {
                                $.ajax({
                                    url: 'http://localhost/api/' + link,
                                    type: 'post',
                                    dataType: 'html',
                                    crossDomain: true,
                                    data: {"token": token},
                                    success: function(data) {
                                        $('main').replaceWith(data);
                                    }
                                });
                            } else {
                                cerrarsesion();
                            }
                        }
                    });
                    if(link == 'mp'){
                        $('footer').hide();
                    } else {
                        $('footer').show();
                    }
                }, function (error) {
                    cerrarsesion();
            }
        );
    });
}