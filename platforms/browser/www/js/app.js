var cliente;

function header(){
    var sidenav;
    if (window.location.href.includes('cuenta')) {
        sidenav = 'true';
    } else {
        sidenav = 'false';
    }
    NativeStorage.getItem(
        "token",
        function(token){ //Success
            $.ajax({
                url: 'http://localhost/api/token',
                type: 'post',
                dataType: 'json',
                data: {"token": token},
                success: function(data) {
                    if (data.success) {
                        $.ajax({
                            url: 'http://localhost/api/header',
                            type: 'post',
                            dataType: 'html',
                            crossDomain: true,
                            data: {
                                token: token,
                                sidenav: sidenav
                            },
                            success: function(data) {
                                $('body').prepend(data);
                                links();
                            }
                        });
                    } else {
                        cerrarsesion();
                    }
                }
            });
        }, function (error) {
            console.log(error);
            $.ajax({
                url: 'http://localhost/api/header',
                type: 'post',
                dataType: 'html',
                crossDomain: true,
                data: {
                    token: 'null',
                    sidenav: sidenav
                },
                success: function(data) {
                    $('body').prepend(data);
                    if(device.platform != "browser"){
                        links();
                    }
                }
            });
        }
    );
}

function cerrarsesion(){
    NativeStorage.remove("token",function(){
        if(device.platform == 'browser') {
            window.location.replace('home.html');
        } else {
            window.plugins.nativepagetransitions.slide({
                "duration": 700,
                "href": "iniciarsesion.html"
            });
        }
    }, function(){
        if(device.platform == 'browser') {
            window.location.replace('home.html');
        } else {
            window.plugins.nativepagetransitions.slide({
                "duration": 700,
                "href": "iniciarsesion.html"
            });
        }
    });
}

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);
    },
    onDeviceReady: function() {
        if (window.location.href.includes('cuenta.html')) {
            cargar('cuenta');
        }
        document.addEventListener('backbutton', function (evt) {
            if (device.platform == 'browser') {
                return;
            }
            if ($('#sidenav-overlay').length > 0) {
                $('.button-collapse').sideNav('hide');
            } else {
                if (!window.location.href.includes('home.html')) {
                    window.history.back();
                } else {
                    if(navigator.app){
                        navigator.app.exitApp();
                    }
                    else if(navigator.device){
                        navigator.device.exitApp();
                    }
                }
            }
        }, false);
        NativeStorage.getItem(
            "token",
            function(token){ //Success
                $.ajax({
                    url: 'http://localhost/api/token',
                    type: 'post',
                    dataType: 'json',
                    data: {"token": token},
                    success: function(data) {
                        if (data.cliente == null) {
                            cerrarsesion();
                        } else {
                            cliente = data.cliente;
                            header();
                        }
                    }
                });
            }, function (error) { //Error
                //No está autenticado.
                if (window.location.href.includes('cuenta')){
                    if(device.platform == 'browser') {
                        window.location.replace('home.html');
                    } else {
                        window.plugins.nativepagetransitions.slide({
                            "duration": 700,
                            "href": "iniciarsesion.html"
                        });
                    }
                }
                header();
            }
        );
    }
};

app.initialize();

function links(){
    if (device.platform != "browser") {
        $("a").each(function(){
            var link = $( this ).attr("href");
            if (link != undefined && !link.includes("javascript") && !link.includes("#")) {
                $(this).attr("href", "javascript:navegar('" + link + "');");
            }
        });
    }
    $('script').each(function () {
        if (this.src.length == 0) {
            $(this).remove();
        }
    });
}

function navegar(link){
    window.plugins.nativepagetransitions.slide({
        "direction": "up",
        "duration": 700,
        "href": link
    });
}