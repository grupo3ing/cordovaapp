(function($){
  $(function(){
  	$('.tooltipped').tooltip({
  		delay: 50,
  		html: true
  	});
  	$('.modal').modal();
  	$('select').material_select();
    $('.button-collapse').sideNav();
	$('.collapsible').collapsible();
    $('.dropdown-button').dropdown({
      	inDuration: 300,
      	outDuration: 225,
      	constrainWidth: true,
      	hover: false,
      	gutter: 0,
      	belowOrigin: true,
      	alignment: 'right',
      	stopPropagation: false
	});
  }); // end of document ready
})(jQuery); // end of jQuery name space

function init() {
  	$('.tooltipped').tooltip({
  		delay: 50,
  		html: true
  	});
	$('.modal').modal();
	$('select').material_select();
    $('.button-collapse').sideNav();
	$('.collapsible').collapsible();
    $('.dropdown-button').dropdown({
      	inDuration: 300,
      	outDuration: 225,
      	constrainWidth: true,
      	hover: false,
      	gutter: 0,
      	belowOrigin: true,
      	alignment: 'right',
      	stopPropagation: false
	});
}