$('#loginForm').on("submit", function (event) {
    $.ajax({
        url: 'http://192.168.43.213/api/login',
        type: 'post',
        dataType: 'json',
        data: $('form#loginForm').serialize(),
        beforeSend: function() {
            $("#dni").prop('disabled', true);
            $("#password").prop('disabled', true);
            $("#login").addClass("disabled");
            $("#login").text('Cargando...');
        },
        success: function(data) {
            if (data.success) {
                NativeStorage.setItem("token", data.token,
                function() {
                    if(device.platform == 'browser') {
                        window.location.replace('home.html');
                    } else {
                        window.plugins.nativepagetransitions.slide({
                            "direction": "right",
                            "duration": 700,
                            "href": "home.html"
                        });
                    }
                }, function() {
                    console.log("No se pudo guardar el token");
                });
            } else {
                $("#login").text('Iniciar sesión');
                $("#dni").prop('disabled', false);
                $("#password").prop('disabled', false);
                $("#login").removeClass("disabled");
                $("#error").text(data.message);
            }
        }
    });
    event.preventDefault();
});