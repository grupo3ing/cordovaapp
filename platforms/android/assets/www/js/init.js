(function($){
  $(function(){
  	/*$('.datepicker').pickadate({
  		changeMonth: false,
  		changeYear: false,
	    selectMonths: 0, // Creates a dropdown to control month
	    selectYears: 0, // Creates a dropdown of 15 years to control year,
	    today: 'Hoy',
	    clear: 'Reiniciar',
	    close: 'Aceptar',
	    closeOnSelect: false // Close upon selecting a date,
	});*/
  	$('.modal').modal();
  	$('select').material_select();
    $('.button-collapse').sideNav();
	$('.collapsible').collapsible();
    $('.dropdown-button').dropdown({
	      	inDuration: 300,
	      	outDuration: 225,
	      	constrainWidth: true, // Does not change width of dropdown to that of the activator
	      	hover: false, // Activate on hover
	      	gutter: 0, // Spacing from edge
	      	belowOrigin: true, // Displays dropdown below the button
	      	alignment: 'right', // Displays dropdown with edge aligned to the right of button
	      	stopPropagation: false // Stops event propagation
	    }
	);
  }); // end of document ready
})(jQuery); // end of jQuery name space

function init() {
	/*$('.datepicker').pickadate({
		changeMonth: false,
  		changeYear: false,
	    selectMonths: 0, // Creates a dropdown to control month
	    selectYears: 0, // Creates a dropdown of 15 years to control year,
	    today: 'Hoy',
	    clear: 'Reiniciar',
	    close: 'Aceptar',
	    closeOnSelect: false // Close upon selecting a date,
	});*/
	$('.modal').modal();
	$('select').material_select();
    $('.button-collapse').sideNav();
	$('.collapsible').collapsible();
    $('.dropdown-button').dropdown({
	      	inDuration: 300,
	      	outDuration: 225,
	      	constrainWidth: true, // Does not change width of dropdown to that of the activator
	      	hover: false, // Activate on hover
	      	gutter: 0, // Spacing from edge
	      	belowOrigin: true, // Displays dropdown below the button
	      	alignment: 'right', // Displays dropdown with edge aligned to the right of button
	      	stopPropagation: false // Stops event propagation
	    }
	);
}